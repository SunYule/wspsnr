/* The copyright in this software is being made available under the BSD
 * License, included below. This software may be subject to other third party
 * and contributor rights, including patent rights, and no such rights are
 * granted under this license.
 *
 * Copyright (c) 2010-2019, ISO/IEC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  * Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  * Neither the name of the ISO/IEC nor the names of its contributors may
 *    be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <cstring>
#include <fstream>
#include "Config.hpp"
#include <iostream>

using namespace std;
#define S_PI 3.14159265358979323846

int main(int argc, char* argv[]) {

	try {

		std::cout
			<< " - -------------------------------------------- -\n"
			<< "|                WS-PSNR software                |\n"
			<< "|    MPEG2018/N18069 WS-PSNR software manual     |\n"
			<< " - -------------------------------------------- -\n" << std::endl;

		if (argc == 1)
			throw std::runtime_error("Usage: WS-PSNR CONFIGURATION_FILE");

		if (argc > 2)
			throw std::logic_error("Too many parameters \n");

		//==========  read and check parameters from json file  ==========  

		std::string filename =  argv[1];

		Config m_config;

		m_config = Config::loadFromFile(filename);

		if (m_config.projection==0 && m_config.latRange / m_config.longRange != 1.0 * m_config.height / m_config.width) 
			throw std::runtime_error("The ratio of height and width should be aligned with ratio of latitude and longitude range \n");

		//==========  finish reading parameters from json file  ==========  

		if (m_config.projection == 1)
			cout <<"\n \nFrame#     PSNR_Y     PSNR_U      PSNR_V\n\n";
		else
			cout << "\n \nFrame#   WS-PSNR_Y   WS-PSNR_U   WS-PSNR_V\n\n";

		//==========  ERP Weight Table  ==========  

		double* ErpWeight_Y;
		double* ErpWeight_C;

		if (m_config.projection == 0)
		{
			ErpWeight_Y = (double*)malloc(m_config.height * sizeof(double));
			ErpWeight_C = (double*)malloc(m_config.height / 2 * sizeof(double));

			double full_ERP_height = 180.0*m_config.height / m_config.latRange;
			double top_offset = (full_ERP_height - m_config.height) / 2.0;

			for (int y = 0; y < m_config.height; y++)
			{
				ErpWeight_Y[y] = cos((y + top_offset - (full_ERP_height / 2 - 0.5))*S_PI / full_ERP_height);
			}
			for (int y = 0; y < m_config.height / 2; y++)
			{
				ErpWeight_C[y] = ErpWeight_Y[2 * y];
			}
		}

		// WS-PSNR calculation

		long long CountFrame;
		double psnr_sphere_Y = 0;
		double psnr_sphere_U = 0;
		double psnr_sphere_V = 0;

		if (m_config.bit_depth == 8) {

			fstream FpOriginalFile;
			fstream FpReconFile;
			FpOriginalFile.open(m_config.original_file.c_str(), ios::binary | ios::in);
			FpReconFile.open(m_config.reconstructed_file.c_str(), ios::binary | ios::in);

			if (!FpOriginalFile)
			{
			   throw std::runtime_error("Fail to open original file \n");
			}

			if (!FpReconFile)
			{
				throw std::runtime_error("Fail to open reconstructed file \n");
			}

			unsigned char *OriginalFileBuffer = (unsigned char *)malloc(sizeof(unsigned char)*m_config.width*m_config.height * 3 / 2);
			unsigned char *ReconFileBuffer = (unsigned char *)malloc(sizeof(unsigned char)*m_config.width*m_config.height * 3 / 2);

			for (CountFrame = 0; CountFrame < m_config.number_of_frames; CountFrame++) {

				cout<<"Frame:" << CountFrame;
				long long offset = m_config.height*m_config.width * 3 / 2 * CountFrame;

				if (CountFrame == 0) {
					const streamoff offset_o = m_config.start_frame_ori*m_config.height*m_config.width * 3 / 2;
					FpOriginalFile.seekg(offset_o, ios::cur);
					const streamoff offset_r = m_config.start_frame_rec*m_config.height*m_config.width * 3 / 2;
					FpReconFile.seekg(offset_r, ios::cur);
				}

				FpOriginalFile.read(reinterpret_cast<char*>(OriginalFileBuffer), m_config.height*m_config.width * 3 / 2);
				FpReconFile.read(reinterpret_cast<char*>(ReconFileBuffer), m_config.height*m_config.width * 3 / 2);

				if (FpOriginalFile.eof())
				{
					printf("\n\nFail to read frame %lld of original file \n", CountFrame + m_config.start_frame_ori);
					exit(1);
				}

				if (FpReconFile.eof())
				{
					printf("\n\nFail to read frame %lld of reconstructed file \n", CountFrame + m_config.start_frame_rec);
					exit(1);
				}

				double ssdR_Y = 0;
				double ssdR_U = 0;
				double ssdR_V = 0;

				double latWeight = 0;
				double totWeight_Y = 0;
				double totWeight_U = 0;
				double totWeight_V = 0;

				for (int j = m_config.valid_rectangular_region_from_top_boundary_In_luma; j < m_config.height - m_config.valid_rectangular_region_from_bottom_boundary_In_luma; j++) {
					for (int i = m_config.valid_rectangular_region_from_left_boundary_In_luma; i < m_config.width - m_config.valid_rectangular_region_from_right_boundary_In_luma; i++) {
						if (m_config.projection == 0)
							latWeight = ErpWeight_Y[j];
						else if (m_config.projection == 1)
							latWeight = 1.0;

						double v = OriginalFileBuffer[j*m_config.width + i] - ReconFileBuffer[j*m_config.width + i];
						ssdR_Y += 100000 * v*v*latWeight;
						totWeight_Y += latWeight;
					}
				}
				double mseR_Y = ssdR_Y / totWeight_Y / 100000;
				double psrR_Y = 10 * log10(255 * 255 / mseR_Y);

				printf("  %.4f", psrR_Y);
				psnr_sphere_Y += psrR_Y;

				for (int j = m_config.valid_rectangular_region_from_top_boundary_In_luma / 2; j < (m_config.height - m_config.valid_rectangular_region_from_bottom_boundary_In_luma) / 2; j++) {
					for (int i = m_config.valid_rectangular_region_from_left_boundary_In_luma / 2; i < (m_config.width - m_config.valid_rectangular_region_from_right_boundary_In_luma) / 2; i++) {
						if (m_config.projection == 0)
							latWeight = ErpWeight_C[j];
						else if (m_config.projection == 1)
							latWeight = 1.0;
						double v = OriginalFileBuffer[m_config.width*m_config.height + j*m_config.width / 2 + i] - ReconFileBuffer[m_config.width*m_config.height + j*m_config.width / 2 + i];

						ssdR_U += 100000 * v*v*latWeight;
						totWeight_U += latWeight;
					}
				}
				double mseR_U = ssdR_U / totWeight_U / 100000;
				double psrR_U = 10 * log10(255 * 255 / mseR_U);

				printf("     %.4f", psrR_U);
				psnr_sphere_U += psrR_U;

				for (int j = m_config.valid_rectangular_region_from_top_boundary_In_luma / 2; j < (m_config.height - m_config.valid_rectangular_region_from_bottom_boundary_In_luma) / 2; j++) {
					for (int i = m_config.valid_rectangular_region_from_left_boundary_In_luma / 2; i < (m_config.width - m_config.valid_rectangular_region_from_right_boundary_In_luma) / 2; i++) {
						if (m_config.projection == 0)
							latWeight = ErpWeight_C[j];
						else if (m_config.projection == 1)
							latWeight = 1.0;
						double v = OriginalFileBuffer[m_config.width*m_config.height * 5 / 4 + j*m_config.width / 2 + i] - ReconFileBuffer[m_config.width*m_config.height * 5 / 4 + j*m_config.width / 2 + i];

						ssdR_V += 100000 * v*v*latWeight;
						totWeight_V += latWeight;
					}
				}
				double mseR_V = ssdR_V / totWeight_V / 100000;
				double psrR_V = 10 * log10(255 * 255 / mseR_V);

				printf("     %.4f\n", psrR_V);
				psnr_sphere_V += psrR_V;
			}
			FpOriginalFile.close();
			FpReconFile.close();
			free(OriginalFileBuffer);
			free(ReconFileBuffer);
		}

		else if (m_config.bit_depth == 10) {

			fstream FpOriginalFile;
			fstream FpReconFile;
			FpOriginalFile.open(m_config.original_file.c_str(), ios::binary | ios::in);
			FpReconFile.open(m_config.reconstructed_file.c_str(), ios::binary | ios::in);

			if (!FpOriginalFile)
			{
				throw std::runtime_error("Fail to open original file \n" );
			}
			if (!FpReconFile)
			{
				throw std::runtime_error("Fail to open reconstructed file \n");
			}

			unsigned char *ori_buf = (unsigned char *)malloc(sizeof(unsigned char)*m_config.width*m_config.height * 3);
			unsigned char *rec_buf = (unsigned char *)malloc(sizeof(unsigned char)*m_config.width*m_config.height * 3);
			unsigned short *OriginalFileBuffer = (unsigned short *)malloc(sizeof(unsigned short)*m_config.width*m_config.height * 3 / 2);
			unsigned short *ReconFileBuffer = (unsigned short *)malloc(sizeof(unsigned short)*m_config.width*m_config.height * 3 / 2);

			for (CountFrame = 0; CountFrame < m_config.number_of_frames; CountFrame++) {
				cout << "Frame:" << CountFrame;

				if (CountFrame == 0) {
					const streamoff offset_o = m_config.start_frame_ori*m_config.height*m_config.width * 3;
					FpOriginalFile.seekg(offset_o, ios::cur);
					const streamoff offset_r = m_config.start_frame_rec*m_config.height*m_config.width * 3;
					FpReconFile.seekg(offset_r, ios::cur);
				}

				FpOriginalFile.read(reinterpret_cast<char*>(ori_buf), m_config.height*m_config.width * 3);
				FpReconFile.read(reinterpret_cast<char*>(rec_buf), m_config.height*m_config.width * 3);

				if (FpOriginalFile.eof())
				{
					printf("\n\nFail to read frame %lld of original file \n", CountFrame + m_config.start_frame_ori);
					exit(1);
				}

				if (FpReconFile.eof())
				{
					printf("\n\nFail to read frame %lld of reconstructed file \n", CountFrame + m_config.start_frame_rec);
					exit(1);
				}

				for (int i = 0; i < m_config.height*m_config.width * 3 / 2; i++)
				{
					OriginalFileBuffer[i] = (unsigned short)(ori_buf[i * 2]) | (unsigned short)((ori_buf[i * 2 + 1]) << 8);
					ReconFileBuffer[i] = (unsigned short)(rec_buf[i * 2]) | (unsigned short)((rec_buf[i * 2 + 1]) << 8);
				}

				double ssdR_Y = 0;
				double ssdR_U = 0;
				double ssdR_V = 0;

				double latWeight = 0;
				double totWeight_Y = 0;
				double totWeight_U = 0;
				double totWeight_V = 0;

				for (int j = m_config.valid_rectangular_region_from_top_boundary_In_luma; j < m_config.height - m_config.valid_rectangular_region_from_bottom_boundary_In_luma; j++) {
					for (int i = m_config.valid_rectangular_region_from_left_boundary_In_luma; i < m_config.width - m_config.valid_rectangular_region_from_right_boundary_In_luma; i++) {
						if (m_config.projection == 0)
							latWeight = ErpWeight_Y[j];
						else if (m_config.projection == 1)
							latWeight = 1.0;
						double v = OriginalFileBuffer[j*m_config.width + i] - ReconFileBuffer[j*m_config.width + i];

						ssdR_Y += 100000 * v*v*latWeight;
						totWeight_Y += latWeight;
					}
				}
				double mseR_Y = ssdR_Y / totWeight_Y / 100000;
				double psrR_Y = 10 * log10((m_config.peak_value_10bits)*(m_config.peak_value_10bits) / mseR_Y);

				printf("  %.4f", psrR_Y);
				psnr_sphere_Y += psrR_Y;

				for (int j = m_config.valid_rectangular_region_from_top_boundary_In_luma / 2; j < (m_config.height - m_config.valid_rectangular_region_from_bottom_boundary_In_luma) / 2; j++) {
					for (int i = m_config.valid_rectangular_region_from_left_boundary_In_luma / 2; i < (m_config.width - m_config.valid_rectangular_region_from_right_boundary_In_luma) / 2; i++) {
						if (m_config.projection == 0)
							latWeight = ErpWeight_C[j];
						else if (m_config.projection == 1)
							latWeight = 1.0;
						double v = OriginalFileBuffer[m_config.width*m_config.height + j*m_config.width / 2 + i] - ReconFileBuffer[m_config.width*m_config.height + j*m_config.width / 2 + i];

						ssdR_U += 100000 * v*v*latWeight;
						totWeight_U += latWeight;
					}
				}
				double mseR_U = ssdR_U / totWeight_U / 100000;
				double psrR_U = 10 * log10((m_config.peak_value_10bits)*(m_config.peak_value_10bits) / mseR_U);

				printf("     %.4f", psrR_U);
				psnr_sphere_U += psrR_U;

				for (int j = m_config.valid_rectangular_region_from_top_boundary_In_luma / 2; j < (m_config.height - m_config.valid_rectangular_region_from_bottom_boundary_In_luma) / 2; j++) {
					for (int i = m_config.valid_rectangular_region_from_left_boundary_In_luma / 2; i < (m_config.width - m_config.valid_rectangular_region_from_right_boundary_In_luma) / 2; i++) {
						if (m_config.projection == 0)
							latWeight = ErpWeight_C[j];
						else if (m_config.projection == 1)
							latWeight = 1.0;
						double v = OriginalFileBuffer[m_config.width*m_config.height * 5 / 4 + j*m_config.width / 2 + i] - ReconFileBuffer[m_config.width*m_config.height * 5 / 4 + j*m_config.width / 2 + i];

						ssdR_V += 100000 * v*v*latWeight;
						totWeight_V += latWeight;
					}
				}
				double mseR_V = ssdR_V / totWeight_V / 100000;
				double psrR_V = 10 * log10((m_config.peak_value_10bits)*(m_config.peak_value_10bits) / mseR_V);

				printf("     %.4f\n", psrR_V);
				psnr_sphere_V += psrR_V;
			}

			FpOriginalFile.close();
			FpReconFile.close();
			free(ori_buf);
			free(rec_buf);
			free(OriginalFileBuffer);
			free(ReconFileBuffer);
		}

		if (m_config.projection == 0)
		{
			free(ErpWeight_Y);
			free(ErpWeight_C);
		}

		psnr_sphere_Y = psnr_sphere_Y / m_config.number_of_frames;
		psnr_sphere_U = psnr_sphere_U / m_config.number_of_frames;
		psnr_sphere_V = psnr_sphere_V / m_config.number_of_frames;

		if (m_config.projection == 1)
			printf("\nAverage PSNRS \n\n");
		else
			printf("\nAverage WS-PSNRS \n\n");
		printf("          %.4f", psnr_sphere_Y);
		printf("     %.4f", psnr_sphere_U);
		printf("     %.4f\n", psnr_sphere_V);

		return 0;
	}

	catch (std::exception& e)
	{
		std::cerr << e.what() << std::endl;
		return 1;
	}
}

